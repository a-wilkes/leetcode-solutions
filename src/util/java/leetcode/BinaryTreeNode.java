package util.java.leetcode;

// Definition of BinaryTreeNode provided by leetcode

public class BinaryTreeNode {
    public int val;               // NOSONAR - external definition
    public BinaryTreeNode left;   // NOSONAR - external definition
    public BinaryTreeNode right;  // NOSONAR - external definition

    public BinaryTreeNode() {}

    public BinaryTreeNode(int val) { this.val = val; }

    public BinaryTreeNode(int val, BinaryTreeNode left, BinaryTreeNode right) {
        this.val   = val;
        this.left  = left;
        this.right = right;
    }
}
