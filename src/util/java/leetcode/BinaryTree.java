package util.java.leetcode;

import java.util.*;

public class BinaryTree {

    private BinaryTree() {}

    public static void preOrder(BinaryTreeNode root) {
        System.out.print(String.format("%-6s", "pre: ")); // NOSONAR - these are just for testing trees
        internalPreOrder(root);
        System.out.println(); // NOSONAR
    }

    public static void inOrder(BinaryTreeNode root) {
        System.out.print(String.format("%-6s", "in: ")); // NOSONAR
        internalInOrder(root);
        System.out.println(); // NOSONAR
    }

    public static void postOrder(BinaryTreeNode root) {
        System.out.print(String.format("%-6s", "post: ")); // NOSONAR
        internalPostOrder(root);
        System.out.println(); // NOSONAR
    }

    private static void internalPreOrder(BinaryTreeNode root) {
        if (root == null) {
            System.out.print("null, "); // NOSONAR
            return;
        }

        System.out.print(String.format("%d, ", root.val)); // NOSONAR
        internalPreOrder(root.left);
        internalPreOrder(root.right);
    }

    private static void internalInOrder(BinaryTreeNode root) {
        if (root == null) {
            System.out.print("null, "); // NOSONAR
            return;
        }

        internalInOrder(root.left);
        System.out.print(String.format("%d, ", root.val)); // NOSONAR
        internalInOrder(root.right);
    }

    private static void internalPostOrder(BinaryTreeNode root) {
        if (root == null) {
            System.out.print("null, "); // NOSONAR
            return;
        }

        internalPostOrder(root.left);
        internalPostOrder(root.right);
        System.out.print(String.format("%d, ", root.val)); // NOSONAR
    }

    public static boolean areEqual(final BinaryTreeNode root1, final BinaryTreeNode root2) {
        if (root1 == null && root2 == null) { return true; }
        if (root1 == null || root2 == null) { return false; }

        List<Integer> tree1 = new ArrayList<>();
        List<Integer> tree2 = new ArrayList<>();

        treeToString(root1, tree1);
        treeToString(root2, tree2);

        return tree1.equals(tree2);
    }

    public static void treeToString(final BinaryTreeNode n, List<Integer> tree) {
        if (n == null) {
            tree.add(null);
            return;
        }

        tree.add(n.val);
        treeToString(n.left, tree);
        treeToString(n.right, tree);
    }

    public static BinaryTreeNode createTree(final List<Integer> treeBlueprint) {
        if (treeBlueprint == null || treeBlueprint.isEmpty()) { return null; }

        return linkNodes(
                createNodes(treeBlueprint),
                treeBlueprint.size()
        ).get(0);
    }

    private static HashMap<Integer, BinaryTreeNode> createNodes(final List<Integer> treeBlueprint) {
        var nodes = new HashMap<Integer, BinaryTreeNode>();

        for (int i = 0; i < treeBlueprint.size(); ++i) {
            if (treeBlueprint.get(i) != null) {
                nodes.put(i, new BinaryTreeNode(treeBlueprint.get(i)));
            }
        }

        return nodes;
    }

    private static HashMap<Integer, BinaryTreeNode> linkNodes(HashMap<Integer, BinaryTreeNode> nodes, final int treeSize) {
        for (int childIndex = treeSize - 1; childIndex > 0; --childIndex) {
            final int parentIndex = childToParentIndex(childIndex);

            BinaryTreeNode parent = nodes.get(parentIndex);
            BinaryTreeNode child  = nodes.get(childIndex);

            addChild(parent, child, childIndex);
        }

        return nodes;
    }

    private static void addChild(BinaryTreeNode parent, final BinaryTreeNode child, final int childIndex) {
        if (parent == null) { return; }

        if (isEven(childIndex)) { parent.right = child; }
        else { parent.left = child; }
    }

    private static int childToParentIndex(final int ci) {
        return (ci - 1) / 2;
    }

    private static boolean isEven(final int i) {
        return i % 2 == 0;
    }

}
