package util.java.leetcode;

// Definition of BinaryTreeNode provided by leetcode

public class ListNode {
    public int val;       // NOSONAR - external definition
    public ListNode next; // NOSONAR - external definition

    public ListNode() {}

    public ListNode(int val) { this.val = val; }

    public ListNode(int val, ListNode next) {
        this.val  = val;
        this.next = next;
    }
}
