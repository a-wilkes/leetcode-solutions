package util.java.leetcode;

import java.util.List;

public class LinkedList {
    private LinkedList() {}

    public static void walkList(final ListNode head) {
        System.out.println("list:"); // NOSONAR - this is just for testing lists

        ListNode current = head;
        while (current != null) {
            System.out.print(String.format("%d, ", current.val)); // NOSONAR
            current = current.next;
        }

        System.out.println(); // NOSONAR
    }

    public static ListNode createList(final List<Integer> listBlueprint) {
        if (listBlueprint == null || listBlueprint.isEmpty()) { return null; }

        ListNode head = new ListNode(listBlueprint.get(0));
        ListNode previous = head;

        for (int i = 1; i < listBlueprint.size(); ++i) {
            ListNode current = new ListNode(listBlueprint.get(i));
            previous.next = current;
            previous = current;
        }

        return head;
    }
}
