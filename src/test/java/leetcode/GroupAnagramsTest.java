package test.java.leetcode;

import main.java.leetcode.GroupAnagrams;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@RunWith(Parameterized.class)
public class GroupAnagramsTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new String[] { "eat", "tea", "tan", "ate", "nat", "bat" },
                        new HashSet<>(Arrays.asList(
                                Arrays.stream(new String[] {"ate", "eat", "tea"}).sorted().collect(Collectors.toList()),
                                Arrays.stream(new String[] {"nat", "tan"}).sorted().collect(Collectors.toList()),
                                Arrays.stream(new String[] {"bat"}).sorted().collect(Collectors.toList())
                        ))
                },
                { new String[] { "", "" }, Collections.singleton(Arrays.asList("", "")) },
                { null, Collections.emptySet() },
                { new String[] {}, Collections.emptySet() }
        });
    }

    @Parameterized.Parameter
    public String[] toCheck;

    @Parameterized.Parameter(1)
    public Set<List<String>> expected;

    @Test
    public void test() {
        Set<List<String>> res = GroupAnagrams.groupAnagrams(toCheck)
                .stream()
                .map(l -> l.stream()
                        .sorted()
                        .collect(Collectors.toList())
                ).collect(Collectors.toSet());
        assertIterableEquals(expected, res);
    }
}
