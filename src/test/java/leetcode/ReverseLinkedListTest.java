package test.java.leetcode;

import main.java.leetcode.ReverseLinkedList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.java.leetcode.LinkedList;
import util.java.leetcode.ListNode;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(Parameterized.class)
public class ReverseLinkedListTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { // NOSONAR - false positive
                { LinkedList.createList(Arrays.asList(1, 2, 3, 4, 5)),
                        LinkedList.createList(Arrays.asList(5, 4, 3, 2, 1)) },
                { null, null },
                { LinkedList.createList(Collections.singletonList(1)),
                        LinkedList.createList(Collections.singletonList(1)) }
        });
    }

    @Parameterized.Parameter
    public ListNode toCheck;

    @Parameterized.Parameter(1)
    public ListNode expected;

    @Test
    public void test() {
        ListNode currentToCheck = ReverseLinkedList.reverseList(toCheck);
        ListNode currentExpected = expected;

        while (currentToCheck != null && currentExpected != null) {
            assertEquals(currentToCheck.val, currentToCheck.val);

            currentToCheck = currentToCheck.next;
            currentExpected = currentExpected.next;
        }

        assertNull(currentToCheck);
        assertNull(currentExpected);
    }
}