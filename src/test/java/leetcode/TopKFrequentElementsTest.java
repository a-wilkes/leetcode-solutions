package test.java.leetcode;

import main.java.leetcode.TopKFrequentElements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

@RunWith(Parameterized.class)
public class TopKFrequentElementsTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { // NOSONAR - false positive
                { new int[] { 1, 1, 1, 2, 2, 3 }, 2, new int[] { 1, 2 } },
                { new int[] { 1 }, 1, new int[] { 1 } },
                { new int[] { 4, 1, -1, 2, -1, 2, 3 }, 2, new int[] { -1, 2 }}
        });
    }

    @Parameterized.Parameter
    public int[] toCheckNums;

    @Parameterized.Parameter(1)
    public int toCheckK;

    @Parameterized.Parameter(2)
    public int[] expected;

    @Test
    public void test() {
        assertArrayEquals(expected, TopKFrequentElements.topKFrequent(toCheckNums, toCheckK));
    }
}
