package test.java.leetcode;

import main.java.leetcode.Permutations;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@RunWith(Parameterized.class)
public class PermutationsTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { // NOSONAR - false positive
                { new int[] { 1, 2, 3 }, Arrays.asList(
                        Arrays.asList(1, 2, 3),
                        Arrays.asList(1, 3, 2),
                        Arrays.asList(2, 1, 3),
                        Arrays.asList(2, 3, 1),
                        Arrays.asList(3, 1, 2),
                        Arrays.asList(3, 2, 1)
                ) },
                { new int[0], Collections.singletonList(Collections.emptyList())},
                { new int[] { 1 }, Collections.singletonList(Collections.singletonList(1)) },
                { new int[] { 1, 2 }, Arrays.asList(
                        Arrays.asList(1, 2),
                        Arrays.asList(2, 1)
                ) },
                { new int[] { 1, 2, 3, 4 }, Arrays.asList(
                        Arrays.asList(1, 2, 3, 4),
                        Arrays.asList(1, 2, 4, 3),
                        Arrays.asList(1, 3, 2, 4),
                        Arrays.asList(1, 3, 4, 2),
                        Arrays.asList(1, 4, 2, 3),
                        Arrays.asList(1, 4, 3, 2),
                        Arrays.asList(2, 1, 3, 4),
                        Arrays.asList(2, 1, 4, 3),
                        Arrays.asList(2, 3, 1, 4),
                        Arrays.asList(2, 3, 4, 1),
                        Arrays.asList(2, 4, 1, 3),
                        Arrays.asList(2, 4, 3, 1),
                        Arrays.asList(3, 1, 2, 4),
                        Arrays.asList(3, 1, 4, 2),
                        Arrays.asList(3, 2, 1, 4),
                        Arrays.asList(3, 2, 4, 1),
                        Arrays.asList(3, 4, 1, 2),
                        Arrays.asList(3, 4, 2, 1),
                        Arrays.asList(4, 1, 2, 3),
                        Arrays.asList(4, 1, 3, 2),
                        Arrays.asList(4, 2, 1, 3),
                        Arrays.asList(4, 2, 3, 1),
                        Arrays.asList(4, 3, 1, 2),
                        Arrays.asList(4, 3, 2, 1)
                ) }
        });
    }

    @Parameterized.Parameter
    public int[] toCheck;

    @Parameterized.Parameter(1)
    public List<List<Integer>> expected;

    @Test
    public void test() {
        assertIterableEquals(expected, Permutations.permute(toCheck));
    }
}
