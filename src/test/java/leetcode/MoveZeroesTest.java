package test.java.leetcode;

import main.java.leetcode.MoveZeroes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

@RunWith(Parameterized.class)
public class MoveZeroesTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new int[] { 0, 1, 0, 3, 12 }, new int[] { 1, 3, 12, 0, 0 } },
                { null, null },
                { new int[] { 1 }, new int[] { 1 } },
                { new int[] { 0 }, new int[] { 0 } },
                { new int[] { 1, 0, 0 }, new int[] { 1, 0, 0 } },
                { new int[] { 0, 0, 1 }, new int[] { 1, 0, 0 } },
                { new int[] { 1, 0, 0, 0 }, new int[] { 1, 0, 0, 0 } },
                { new int[] { 0, 0, 0, 1 }, new int[] { 1, 0, 0, 0 } }
        });
    }

    @Parameterized.Parameter
    public int[] toCheck;

    @Parameterized.Parameter(1)
    public int[] expected;

    @Test
    public void test() {
        MoveZeroes.moveZeroes(toCheck);
        assertArrayEquals(toCheck, expected);
    }
}