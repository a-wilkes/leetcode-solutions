package test.java.leetcode;

import main.java.leetcode.KThSmallestElementInABst;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.java.leetcode.BinaryTree;
import util.java.leetcode.BinaryTreeNode;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class KThSmallestElementInABstTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { BinaryTree.createTree(Arrays.asList(3 , 1, 4, null, 2)), 1, 1 },
                { BinaryTree.createTree(Arrays.asList(5, 3, 6, 2, 4, null, null, 1)), 3, 3 }
        });
    }

    @Parameterized.Parameter
    public BinaryTreeNode rootToCheck;

    @Parameterized.Parameter(1)
    public int kToCheck;

    @Parameterized.Parameter(2)
    public int expected;

    @Test
    public void test() {
        assertEquals(expected, KThSmallestElementInABst.kthSmallest(rootToCheck, kToCheck));
    }
}
