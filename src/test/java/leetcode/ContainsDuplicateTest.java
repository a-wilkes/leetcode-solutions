package test.java.leetcode;

import main.java.leetcode.ContainsDuplicate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class ContainsDuplicateTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new int[] { 1, 2, 3, 1 }, true },
                { new int[] { 1, 2, 3, 4 }, false },
                { new int[] { 1, 1, 1, 3, 3, 4, 3, 2, 4, 2 }, true },
                { null, false },
                { new int[] {}, false },
                { new int[] { 1 }, false }
        });
    }

    @Parameterized.Parameter
    public int[] toCheck;

    @Parameterized.Parameter(1)
    public boolean expected;

    @Test
    public void test() {
        assertEquals(expected, ContainsDuplicate.containsDuplicate(toCheck));
    }
}
