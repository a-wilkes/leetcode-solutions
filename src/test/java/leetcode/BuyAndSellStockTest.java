package test.java.leetcode;

import main.java.leetcode.BuyAndSellStock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class BuyAndSellStockTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new int[] {}, 0 },
                { null, 0 },
                { new int[] { 7, 1, 5, 3, 6, 4 }, 7},
                { new int[] { 1, 2, 3, 4, 5 }, 4},
                { new int[] { 7, 6, 4, 3, 1 }, 0}
        });
    }

    @Parameterized.Parameter
    public int[] toCheck;

    @Parameterized.Parameter(1)
    public int expected;

    @Test
    public void test() {
        assertEquals(expected, BuyAndSellStock.maxProfit(toCheck));
    }
}
