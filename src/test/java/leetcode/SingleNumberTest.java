package test.java.leetcode;

import main.java.leetcode.SingleNumber;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class SingleNumberTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { // NOSONAR - false positive
                { new int[] { 2, 2, 1 }, 1 },
                { new int[] { 4, 1, 2, 1, 2 }, 4 },
                { new int[] { 1 }, 1 }
        });
    }

    @Parameterized.Parameter
    public int[] toCheck;

    @Parameterized.Parameter(1)
    public int expected;

    @Test
    public void test() {
        assertEquals(expected, SingleNumber.singleNumber(toCheck));
    }
}
