package test.java.leetcode;

import main.java.leetcode.SortedArrayToBst;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.java.leetcode.BinaryTree;

import java.util.*;

@RunWith(Parameterized.class)
public class SortedArrayToBstTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new int[] { -10, -3, 0, 5, 9 }, Arrays.asList(0, -10, null, -3, null, null, 5, null, 9, null, null) },
                { new int[] { 0 }, Arrays.asList(0, null, null) },
                { new int[] { -10, -3, -1, 0, 5, 9, 10, 11, 12 },
                        Arrays.asList(5, -3, -10, null, null, -1, null, 0, null, null, 10, 9, null, null, 11, null, 12, null, null) }
        });
    }

    @Parameterized.Parameter
    public int[] toCheck;

    @Parameterized.Parameter(1)
    public List<Integer> expected;

    @Test
    public void test() {
        List<Integer> res = new ArrayList<>();
        BinaryTree.treeToString(SortedArrayToBst.sortedArrayToBST(toCheck), res);
        System.out.println(res.toString());
        Assertions.assertIterableEquals(expected, res);
    }
}
