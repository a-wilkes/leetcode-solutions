package test.java.leetcode;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TestRunner {
    private static final Logger logger;

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%4$-7s] %5$s %n");
        logger = Logger.getLogger(TestRunner.class.getName());
    }

    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(KThSmallestElementInSortedMatrixTest.class);

        for (Failure f : result.getFailures()) { logger.log(Level.SEVERE, "{0}", f); }

        if (result.wasSuccessful()) { logger.info("All tests passed."); }
        else { logger.log(Level.SEVERE, "{0} test(s) failed.", result.getFailureCount()); }
    }
}
