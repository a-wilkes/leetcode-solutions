package test.java.leetcode;

import main.java.leetcode.MajorityElement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class MajorityElementTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new int[] { 3, 2, 3}, 3 },
                { new int[] { 2, 2, 1, 1, 1, 2, 2 }, 2 },
                { new int[] { 1 }, 1 }
        });
    }

    @Parameterized.Parameter
    public int[] toCheck;

    @Parameterized.Parameter(1)
    public int expected;

    @Test
    public void test() {
        assertEquals(expected, MajorityElement.majorityElement(toCheck));
    }
}
