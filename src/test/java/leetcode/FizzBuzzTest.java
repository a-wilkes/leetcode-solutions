package test.java.leetcode;

import main.java.leetcode.FizzBuzz;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@RunWith(Parameterized.class)
public class FizzBuzzTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { // NOSONAR - false positive
                { 15, Arrays.asList(
                        "1",    "2",    "Fizz", "4",    "Buzz",
                        "Fizz", "7",    "8",    "Fizz", "Buzz",
                        "11",   "Fizz", "13",   "14",   "FizzBuzz"
                ) },
                { 0, Collections.emptyList() }
        });
    }

    @Parameterized.Parameter
    public int toCheck;

    @Parameterized.Parameter(1)
    public List<String> expected;

    @Test
    public void test() {
        assertIterableEquals(expected, FizzBuzz.fizzBuzz(toCheck));
    }
}
