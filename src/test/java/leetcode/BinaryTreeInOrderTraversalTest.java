package test.java.leetcode;

import main.java.leetcode.BinaryTreeInOrderTraversal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.java.leetcode.BinaryTree;
import util.java.leetcode.BinaryTreeNode;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@RunWith(Parameterized.class)
public class BinaryTreeInOrderTraversalTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { // NOSONAR - false positive
                { Arrays.asList(1, null, 2, null, null, 3), Arrays.asList(1, 3, 2) },
                { null, Collections.emptyList() }
        });
    }

    @Parameterized.Parameter
    public List<Integer> toCheck;

    @Parameterized.Parameter(1)
    public List<Integer> expected;

    @Test
    public void test() {
        final BinaryTreeNode root = BinaryTree.createTree(toCheck);
        assertIterableEquals(expected, BinaryTreeInOrderTraversal.inOrderTraversal(root));
    }
}
