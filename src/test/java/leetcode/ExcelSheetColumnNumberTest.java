package test.java.leetcode;

import main.java.leetcode.ExcelSheetColumnNumber;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class ExcelSheetColumnNumberTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { "A", 1 }, { "AB", 28 }, { "ZY", 701 }, { "AAA", 703 }
        });
    }

    @Parameterized.Parameter
    public String toCheck;

    @Parameterized.Parameter(1)
    public int expected;

    @Test
    public void test() {
        assertEquals(expected, ExcelSheetColumnNumber.titleToNumber(toCheck));
    }
}
