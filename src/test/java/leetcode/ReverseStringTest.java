package test.java.leetcode;

import main.java.leetcode.ReverseString;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

@RunWith(Parameterized.class)
public class ReverseStringTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { // NOSONAR - false positive
                { "hello".toCharArray(), "olleh".toCharArray() },
                { new char[0], new char[0] },
                { "1234".toCharArray(), "4321".toCharArray() },
                { "Hello".toCharArray(), "olleH".toCharArray() }
        });
    }

    @Parameter
    public char[] toCheck;

    @Parameter(1)
    public char[] expected;

    @Test
    public void test() {
        ReverseString.reverseString(toCheck);
        assertArrayEquals(expected, toCheck);
    }
}