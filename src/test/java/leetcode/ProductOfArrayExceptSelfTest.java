package test.java.leetcode;

import main.java.leetcode.ProductOfArrayExceptSelf;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

@RunWith(Parameterized.class)
public class ProductOfArrayExceptSelfTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new int[] { 1, 2, 3, 4 }, new int[] {24, 12, 8, 6 } },
                { new int[] { 1, 1 }, new int[] { 1, 1 } },
                { new int[] { -2, 2, 2, -2 }, new int[] { -8, 8, 8, -8 }}
        });
    }

    @Parameterized.Parameter
    public int[] toCheck;

    @Parameterized.Parameter(1)
    public int[] expected;

    @Test
    public void test() {
        assertArrayEquals(expected, ProductOfArrayExceptSelf.productExceptSelf(toCheck));
    }
}
