package test.java.leetcode;

import main.java.leetcode.OddEvenLinkedList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.java.leetcode.LinkedList;
import util.java.leetcode.ListNode;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@RunWith(Parameterized.class)
public class OddEvenLinkedListTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { LinkedList.createList(Arrays.asList(1, 2, 3, 4, 5)),
                        LinkedList.createList(Arrays.asList(1, 3, 5, 2, 4))
                },
                { LinkedList.createList(Arrays.asList(2, 1, 3, 5, 6, 4, 7)),
                        LinkedList.createList(Arrays.asList(2, 3, 6, 7, 1, 5, 4))
                },
                { null,
                        null
                },
                { LinkedList.createList(Collections.singletonList(1)),
                        LinkedList.createList(Collections.singletonList(1))
                },
                { LinkedList.createList(Arrays.asList(1, 2)),
                        LinkedList.createList(Arrays.asList(1, 2))
                },
                { LinkedList.createList(Arrays.asList(1, 2, 3)),
                        LinkedList.createList(Arrays.asList(1, 3, 2))
                },
                { LinkedList.createList(Collections.emptyList()),
                        LinkedList.createList(Collections.emptyList())
                }
        });
    }

    @Parameterized.Parameter
    public ListNode toCheck;

    @Parameterized.Parameter(1)
    public ListNode expected;

    @Test
    public void test() {
        ListNode currentToCheck  = OddEvenLinkedList.oddEvenList(toCheck);
        ListNode currentExpected = expected;

        while (currentExpected != null && currentToCheck != null) {
            assertEquals(currentExpected.val, currentToCheck.val);

            currentExpected = currentExpected.next;
            currentToCheck  = currentToCheck.next;
        }

        assertNull(currentExpected);
        assertNull(currentToCheck);
    }
}
