package test.java.leetcode;

import main.java.leetcode.KThSmallestElementInSortedMatrix;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class KThSmallestElementInSortedMatrixTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new int[][] {
                        {  1,  5,  9 },
                        { 10, 11, 13 },
                        { 12, 13, 15 }},
                        8, 13
                },
                { new int [][] { { 1 } }, 1, 1 },
                { new int[][] {
                        {  1,  5,  9 },
                        { 10, 11, 13 },
                        { 12, 14, 15 }},
                        8, 14
                },
                { new int[][] {
                        {  1,  5,  9 },
                        { 10, 11, 14 },
                        { 12, 13, 15 }},
                        8, 14
                }
        });
    }

    @Parameterized.Parameter
    public int[][] matrixToCheck;

    @Parameterized.Parameter(1)
    public int kToCheck;

    @Parameterized.Parameter(2)
    public int expected;

    @Test
    public void test() {
        assertEquals(expected, KThSmallestElementInSortedMatrix.kthSmallest(matrixToCheck, kToCheck));
    }
}
