package test.java.leetcode;

import main.java.leetcode.MaxDepthBinaryTree;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import util.java.leetcode.BinaryTreeNode;
import util.java.leetcode.BinaryTree;

import java.util.Collection;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class MaxDepthBinaryTreeTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { // NOSONAR - false positive
                { Arrays.asList(3, 9, 20, null, null, 15, 7), 3 },
                { null, 0 },
                { Collections.singletonList(0), 1 },
                { Arrays.asList(0, 1), 2 },
                { Arrays.asList(0, null), 1 },
                { Arrays.asList(0, 1, 2), 2 },
                { Arrays.asList(0, null, 2), 2 },
                { Arrays.asList(0, 1, null), 2 },
                { Arrays.asList(0, 1, 2, 3), 3 },
                { Arrays.asList(0, 1, null, 3), 3 },
                { Arrays.asList(0, 1, 2, null), 2 },
                { Arrays.asList(0, 1, 2, 3), 3 }
        });
    }

    @Parameter
    public List<Integer> toCheck;

    @Parameter(1)
    public int expected;

    @Test
    public void test() {
        final BinaryTreeNode root = BinaryTree.createTree(toCheck);
        assertEquals(expected, MaxDepthBinaryTree.maxDepthBinaryTree(root));
    }
}
