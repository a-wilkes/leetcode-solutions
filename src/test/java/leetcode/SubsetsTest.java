package test.java.leetcode;

import main.java.leetcode.Subsets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@RunWith(Parameterized.class)
public class SubsetsTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new int[] { 1, 2, 3 }, Arrays.asList(
                        Collections.singletonList(1),
                        Arrays.asList(1,2),
                        Arrays.asList(1,2,3),
                        Arrays.asList(1,3),
                        Collections.singletonList(2),
                        Arrays.asList(2,3),
                        Collections.singletonList(3),
                        Collections.emptyList()
                )},
                { new int[0], Collections.singletonList(Collections.emptyList()) }
        });
    }

    @Parameterized.Parameter
    public int[] toCheck;

    @Parameterized.Parameter(1)
    public List<List<Integer>> expected;

    @Test
    public void test() {
        assertIterableEquals(expected, Subsets.subsets(toCheck));
    }
}