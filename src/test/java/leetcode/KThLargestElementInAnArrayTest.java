package test.java.leetcode;

import main.java.leetcode.KThLargestElementInAnArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class KThLargestElementInAnArrayTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { new int[] { 3, 2, 1, 5, 6, 4 }, 2, 5 },
                { new int[] { 3, 2, 3, 1, 2, 4, 5, 5, 6 }, 4, 4 },
                { new int[] { 1 }, 1, 1 },
                { new int[] { 1, 1 }, 2, 1 }
        });
    }

    @Parameterized.Parameter
    public int[] toCheckArray;

    @Parameterized.Parameter(1)
    public int toCheckK;

    @Parameterized.Parameter(2)
    public int expected;

    @Test
    public void test() {
        assertEquals(expected, KThLargestElementInAnArray.findKthLargest(toCheckArray, toCheckK));
    }
}
