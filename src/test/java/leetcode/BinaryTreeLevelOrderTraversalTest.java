package test.java.leetcode;

import main.java.leetcode.BinaryTreeLevelOrderTraversal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.java.leetcode.BinaryTree;
import util.java.leetcode.BinaryTreeNode;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@RunWith(Parameterized.class)
public class BinaryTreeLevelOrderTraversalTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { BinaryTree.createTree(Arrays.asList(3, 9, 20, null, null, 15, 7)),
                        Arrays.asList(
                                Collections.singletonList(3),
                                Arrays.asList(9, 20),
                                Arrays.asList(15, 7)
                        )
                },
                { BinaryTree.createTree(Arrays.asList(3, 9, 20, 10, 65, 15, 7)),
                        Arrays.asList(
                                Collections.singletonList(3),
                                Arrays.asList(9, 20),
                                Arrays.asList(10, 65, 15, 7)
                        )
                },
                { null, Collections.emptyList() },
                { BinaryTree.createTree(Collections.singletonList(3)),
                        Collections.singletonList(Collections.singletonList(3))
                },
                { BinaryTree.createTree(Arrays.asList(3, 9, 20, 10, 65, null, 7, 5, null, null, 6)),
                        Arrays.asList(
                                Collections.singletonList(3),
                                Arrays.asList(9, 20),
                                Arrays.asList(10, 65, 7),
                                Arrays.asList(5, 6)
                        )
                }
        });
    }

    @Parameterized.Parameter
    public BinaryTreeNode toCheck;

    @Parameterized.Parameter(1)
    public List<List<Integer>> expected;

    @Test
    public void test() {
        assertIterableEquals(expected, BinaryTreeLevelOrderTraversal.levelOrder(toCheck));
    }
}
