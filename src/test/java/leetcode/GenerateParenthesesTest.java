package test.java.leetcode;

import main.java.leetcode.GenerateParentheses;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@RunWith(Parameterized.class)
public class GenerateParenthesesTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { // NOSONAR - false positive
                { 3, Arrays.asList("()()()", "()(())", "(())()", "(()())", "((()))") },
                { 0, Collections.emptyList() },
                { 1, Collections.singletonList("()") },
                { 2, Arrays.asList("()()", "(())") }
        });
    }

    @Parameterized.Parameter
    public int toCheck;

    @Parameterized.Parameter(1)
    public List<String> expected;

    @Test
    public void test() {
        assertIterableEquals(expected, GenerateParentheses.generateParentheses(toCheck));
    }
}
