package test.java.leetcode;

import main.java.leetcode.RomanToInteger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class RomanToIntegerTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { "I",     1 }, { "II",   2 },
                { "III",   3 }, { "IV",   4 },
                { "V",     5 }, { "VI",   6 },
                { "VII",   7 }, { "VIII", 8 },
                { "IX",    9 }, { "X",   10 },
                { "XI",   11 }, { "XL",  40 },
                { "LX",   60 }, { "XC",  90 },
                { "CX",  110 }, { "CD", 400 },
                { "DC",  600 }, { "CM", 900 },
                { "MC", 1100 },
                { "LVIII",       58},
                { "MCMXCIV",   1994},
                { "MMMCMXCIX", 3999}
        });
    }

    @Parameterized.Parameter
    public String toCheck;

    @Parameterized.Parameter(1)
    public int expected;

    @Test
    public void test() {
        assertEquals(expected, RomanToInteger.romanToInt(toCheck));
    }
}
