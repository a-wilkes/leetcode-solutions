package test.java.leetcode;

import main.java.leetcode.ValidAnagram;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ValidAnagramTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{ // NOSONAR - false positive
                { "anagram", "nagaram", true },
                { "rat", "car", false},
                { "هذه جملة اختبار", "اختبار هذه جملة", true },
                { "هذه جملة اختبار", "هذه جلة اختبار", false }
        });
    }

    @Parameterized.Parameter
    public String baseInput;

    @Parameterized.Parameter(1)
    public String toCheck;

    @Parameterized.Parameter(2)
    public boolean expected;

    @Test
    public void test() {
        Assertions.assertEquals(expected, ValidAnagram.isAnagram(baseInput, toCheck));
    }
}
