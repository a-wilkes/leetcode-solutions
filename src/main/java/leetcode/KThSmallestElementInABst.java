package main.java.leetcode;

import util.java.leetcode.BinaryTreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// https://leetcode.com/problems/kth-smallest-element-in-a-bst/solution/

public class KThSmallestElementInABst {
    private KThSmallestElementInABst() {}

    public static int kthSmallest(BinaryTreeNode root, int k) {
        return inOrder(root).get(k - 1);
    }

    private static List<Integer> inOrder(final BinaryTreeNode n) {
        if (n == null) { return Collections.emptyList(); }

        List<Integer> tree = new ArrayList<>();

        tree.addAll(inOrder(n.left));
        tree.add(n.val);
        tree.addAll(inOrder(n.right));

        return tree;
    }
}