package main.java.leetcode;

// https://leetcode.com/problems/excel-sheet-column-number

public class ExcelSheetColumnNumber {
    private ExcelSheetColumnNumber() {}

    public static int titleToNumber(String s) {
        final int alphabetLength = 26;

        int res = 0;

        for (int i = 0; i < s.length(); ++i) {
            final int letterValue = s.charAt(i) - 'A' + 1;
            res = (res * alphabetLength) + letterValue;
        }

        return res;
    }
}
