package main.java.leetcode;

import java.util.List;
import java.util.stream.Collectors;

// https://leetcode.com/problems/valid-anagram/

public class ValidAnagram {
    private ValidAnagram() {}

    public static boolean isAnagram(String s, String t) {
        if (s == null || t == null || s.length() != t.length()) { return false; }

        List<Integer> sCodePoints = s.codePoints().boxed().sorted().collect(Collectors.toList());
        List<Integer> tCodePoints = t.codePoints().boxed().sorted().collect(Collectors.toList());

        return sCodePoints.equals(tCodePoints);
    }
}
