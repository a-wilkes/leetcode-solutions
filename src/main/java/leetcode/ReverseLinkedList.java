package main.java.leetcode;

// https://leetcode.com/problems/reverse-linked-list/

import util.java.leetcode.ListNode;

public class ReverseLinkedList {
    private ReverseLinkedList() {}

    public static ListNode reverseList(ListNode head) {
        return reverseListRecursive(head, null);
    }

    private static ListNode reverseListIterative(final ListNode head) {
        ListNode current  = head;
        ListNode previous = null;

        while (current != null) {
            ListNode next = current.next;
            current.next  = previous;

            previous = current;
            current  = next;
        }

        return previous;
    }

    private static ListNode reverseListRecursive(ListNode current, ListNode previous) {
        if (current == null) { return previous; }

        ListNode next = current.next;
        current.next  = previous;

        return reverseListRecursive(next, current);
    }
}
