package main.java.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// https://leetcode.com/problems/subsets/

public class Subsets {
    private Subsets() {}

    public static List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> permutations = new ArrayList<>();

        for (int i = 0; i < nums.length; ++i) {
            permutations.addAll(backtrack(new ArrayList<>(), i, nums));
        }

        permutations.add(new ArrayList<>());

        return permutations;
    }

    private static List<List<Integer>> backtrack(List<Integer> permutation, final int index, final int[] nums) {
        if (index == nums.length) { return Collections.emptyList(); }

        List<List<Integer>> permutations = new ArrayList<>();
        List<Integer> newPermutation = new ArrayList<>(permutation);

        newPermutation.add(nums[index]);
        permutations.add(newPermutation);

        for (int i = index + 1; i < nums.length; ++i) {
            permutations.addAll(backtrack(newPermutation, i, nums));
        }

        return permutations;
    }
}
