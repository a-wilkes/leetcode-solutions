package main.java.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// https://leetcode.com/problems/generate-parentheses/

public class GenerateParentheses {
    private GenerateParentheses() {}

    public static List<String> generateParentheses(int n) {
        if (n == 0) { return new ArrayList<>(); }
        return backtrack("", n, 0);
    }

    private static List<String> backtrack(String permutation, final int nToOpen, final int nOpen) {
        if (nToOpen == 0 && nOpen == 0) { return Collections.singletonList(permutation); }

        if (nToOpen == 0) {
            return Collections.singletonList(
                    permutation.concat(")".repeat(nOpen))
            );
        }

        List<String> permutations = new ArrayList<>();

        if (nOpen > 0) {
            permutations.addAll(
                    backtrack(
                            permutation.concat(")"), nToOpen, nOpen - 1)
            );
        }

        permutations.addAll(
                backtrack(
                        permutation.concat("("), nToOpen - 1, nOpen + 1)
        );

        return permutations;
    }
}
