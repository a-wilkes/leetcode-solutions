package main.java.leetcode;

// https://leetcode.com/problems/find-the-duplicate-number/

public class FindTheDuplicateNumber {
    private FindTheDuplicateNumber() {}

// FLoyd's cycle-finding algorithm: https://en.wikipedia.org/wiki/Cycle_detection#Floyd's_Tortoise_and_Hare

    public static int findDuplicate(int[] nums) {
        int tortoise = nums[0];
        int hare = nums[0];

        do {
            tortoise = nums[tortoise];
            hare = nums[nums[hare]];
        } while (tortoise != hare);

        tortoise = nums[0];
        while (tortoise != hare) {
            tortoise = nums[tortoise];
            hare = nums[hare];
        }

        return hare;
    }
}
