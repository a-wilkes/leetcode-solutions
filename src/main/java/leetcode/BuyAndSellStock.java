package main.java.leetcode;

// https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/

public class BuyAndSellStock {
    private BuyAndSellStock() {}

    public static int maxProfit(int[] prices) {
        if (prices == null) { return 0; }

        int profit = 0;
        for (int i = 1; i < prices.length; ++i) {
            final int difference = prices[i] - prices[i - 1];
            profit += Math.max(0, difference);
        }

        return profit;
    }
}
