package main.java.leetcode;

import util.java.leetcode.BinaryTreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// https://leetcode.com/problems/binary-tree-inorder-traversal/

public class BinaryTreeInOrderTraversal {

    private BinaryTreeInOrderTraversal() {}

    public static List<Integer> inOrderTraversal(final BinaryTreeNode root) {
        List<Integer> output = new ArrayList<>();

        Stack<BinaryTreeNode> stack = new Stack<>();

        stack.push(root);

        while (!stack.isEmpty()) {
            BinaryTreeNode n = stack.pop();

            if (n == null) {
                if (!stack.isEmpty()) { n = stack.pop(); }
                if (n != null) { output.add(n.val); }
            } else {
                stack.push(n.right);
                stack.push(n);
                stack.push(n.left);
            }
        }

        return output;
    }
}
