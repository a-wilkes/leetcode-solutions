package main.java.leetcode;

import java.util.ArrayList;
import java.util.List;

// https://leetcode.com/problems/permutations/

public class Permutations {
    private Permutations() {}

    public static List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> permutations = new ArrayList<>();

        internalPermute(permutations, new ArrayList<>(), nums);

        return permutations;
    }

    private static void internalPermute(List<List<Integer>> permutations, List<Integer> permutation, int[] nums) {
        if (permutation.size() == nums.length) {
            permutations.add(new ArrayList<>(permutation));
            return;
        }

        for (final int i : nums) {
            if (permutation.contains(i)) { continue; }

            permutation.add(i);
            internalPermute(permutations, permutation, nums);
            permutation.remove(permutation.size() - 1); // NOSONAR - false positive
        }
    }
}
