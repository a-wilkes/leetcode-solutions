package main.java.leetcode;

import util.java.leetcode.BinaryTreeNode;

import java.util.*;

public class BinaryTreeLevelOrderTraversal {
    private BinaryTreeLevelOrderTraversal() {}

    public static List<List<Integer>> levelOrder(BinaryTreeNode root) {
        List<List<Integer>> result = new ArrayList<>();

        internalLevelOrder(result, root, 0);

        return result;
    }

    private static void internalLevelOrder(List<List<Integer>> result, final BinaryTreeNode current, final int level) {
        if (current == null) { return; }

        if (result.size() == level) { result.add(new ArrayList<>()); }
        result.get(level).add(current.val);

        internalLevelOrder(result, current.left,  level + 1);
        internalLevelOrder(result, current.right, level + 1);
    }
}
