package main.java.leetcode;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// https://leetcode.com/problems/top-k-frequent-elements/

public class TopKFrequentElements {
    private TopKFrequentElements() {}

    public static int[] topKFrequent(int[] nums, int k) {
        return getFrequencies(nums)
                .sorted(Comparator.comparing(e -> -e.getValue()))
                .limit(k)
                .mapToInt(Map.Entry::getKey)
                .toArray();
    }

    private static Stream<Map.Entry<Integer, Integer>> getFrequencies(final int[] nums) {
        return Arrays.stream(nums)
                .boxed()
                .collect(Collectors.toMap(
                        n -> n,
                        n -> 0,
                        (first, second) -> first + 1)
                ).entrySet().stream();
    }
}
