package main.java.leetcode;

import java.util.*;

// https://leetcode.com/problems/group-anagrams/

public class GroupAnagrams {
    private GroupAnagrams() {}

    public static List<List<String>> groupAnagrams(String[] strs) {
        if (strs == null) { return Collections.emptyList(); }

        Map<String, List<String>> sets = new HashMap<>();
        for (final String s : strs) {
            final String sorted = s.chars().sorted()
                    .collect(
                            StringBuilder::new,
                            StringBuilder::appendCodePoint,
                            StringBuilder::append
                    ).toString();

            sets.computeIfAbsent(sorted, k -> new ArrayList<>()).add(s);
        }

        return new ArrayList<>(sets.values());
    }
}
