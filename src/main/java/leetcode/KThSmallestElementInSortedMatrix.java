package main.java.leetcode;

import java.util.Arrays;

public class KThSmallestElementInSortedMatrix {
    private KThSmallestElementInSortedMatrix() {}

    public static int kthSmallest(int[][] matrix, int k) {
        return Arrays.stream(matrix)
                .flatMapToInt(Arrays::stream)
                .sorted()
                .toArray()[k - 1];
    }
}
