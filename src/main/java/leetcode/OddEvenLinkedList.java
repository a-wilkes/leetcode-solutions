package main.java.leetcode;

import util.java.leetcode.ListNode;

// https://leetcode.com/problems/odd-even-linked-list/

public class OddEvenLinkedList {
    private OddEvenLinkedList() {}

    public static ListNode oddEvenList(ListNode head) {
        if (head == null) { return null; }

        ListNode oddTail  = head;
        ListNode evenHead = head.next;
        ListNode evenTail = evenHead;

        while (evenTail != null && evenTail.next != null) {
            oddTail.next = evenTail.next;
            oddTail = oddTail.next;
            evenTail.next = oddTail.next;
            evenTail = evenTail.next;
        }

        oddTail.next = evenHead;

        return head;
    }
}
