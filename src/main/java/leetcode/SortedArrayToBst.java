package main.java.leetcode;

import util.java.leetcode.BinaryTreeNode;

// https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/

public class SortedArrayToBst {
    private SortedArrayToBst() {}

    public static BinaryTreeNode sortedArrayToBST(int[] nums) {
        if (nums == null || nums.length == 0) { return null; }

        return buildSubTree(nums, 0, nums.length - 1);
    }

    private static BinaryTreeNode buildSubTree(final int[] nums, final int lo, final int hi) {
        if (lo > hi) { return null; }

        final int mid = lo + ((hi - lo) / 2);
        return new BinaryTreeNode(
                nums[mid],
                buildSubTree(nums, lo, mid - 1),
                buildSubTree(nums, mid + 1, hi)
        );
    }
}
