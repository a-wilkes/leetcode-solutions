package main.java.leetcode;

// https://leetcode.com/problems/reverse-string/

public class ReverseString {
    private ReverseString() {}

    public static void reverseString(char[] input) {
        final char[] reversed =
                new StringBuilder(String.valueOf(input))
                        .reverse()
                        .toString()
                        .toCharArray();

        System.arraycopy(
                reversed, 0,
                input, 0,
                reversed.length
        );
    }
}
