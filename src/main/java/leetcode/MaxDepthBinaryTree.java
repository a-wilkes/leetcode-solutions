package main.java.leetcode;

// https://leetcode.com/problems/maximum-depth-of-binary-tree/

import util.java.leetcode.BinaryTreeNode;

public class MaxDepthBinaryTree {

    private MaxDepthBinaryTree() {}

    public static int maxDepthBinaryTree(BinaryTreeNode root) {
        return determineDepth(1, root);
    }

    public static int determineDepth(final int depth, final BinaryTreeNode n) {
        if (n == null) { return (depth - 1); }

        final int leftDepth  = determineDepth(depth + 1, n.left);
        final int rightDepth = determineDepth(depth + 1, n.right);

        return Math.max(leftDepth, rightDepth);
    }
}
