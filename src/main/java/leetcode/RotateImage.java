package main.java.leetcode;

// https://leetcode.com/problems/rotate-image/

public class RotateImage {
    private RotateImage() {}

    public static void rotate(int[][] matrix) {
        if (matrix == null) { return; }

        reverse(matrix);

        for (int i = 0; i < matrix.length; ++i) {
            for (int j = i + 1; j < matrix.length; ++j) {
                final int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
    }

    private static void reverse(int[][] m) {
        for (int i = 0; i < m.length / 2; ++i) {
            final int[] temp = m[i];
            m[i] = m[m.length - 1 - i];
            m[m.length - 1 - i] = temp;
        }
    }
}
