package main.java.leetcode;

import java.util.ArrayList;
import java.util.HashSet;

public class SingleNumber {
    private SingleNumber() {}

    public static int singleNumber(int[] nums) {
        HashSet<Integer> s = new HashSet<>();

        for (int i : nums) {
            if (s.contains(i)) { s.remove(i); }
            else { s.add(i); }
        }

        return new ArrayList<>(s).get(0);
    }
}
