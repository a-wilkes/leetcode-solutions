package main.java.leetcode;


// https://leetcode.com/problems/product-of-array-except-self/

public class ProductOfArrayExceptSelf {
    private ProductOfArrayExceptSelf() {}

    public static int[] productExceptSelf(int[] nums) {
        int[] productsLeft  = new int[nums.length];
        int[] productsRight = new int[nums.length];

        int leftProduct  = 1;
        int rightProduct = 1;
        for (int i = 0; i < nums.length; ++i) {
            leftProduct  *= nums[i];
            rightProduct *= nums[nums.length - 1 - i];

            productsLeft[i]  = leftProduct;
            productsRight[productsRight.length - 1 - i] = rightProduct;
        }

        int[] output = new int[nums.length];
        for (int i = 1; i < nums.length - 1; ++i) {
            output[i] = productsLeft[i - 1] * productsRight[i + 1];
        }

        output[0] = productsRight[1];
        output[output.length - 1] = productsLeft[productsLeft.length - 2];

        return output;
    }
}
