package main.java.leetcode;

import java.util.Arrays;

// https://leetcode.com/problems/move-zeroes/

public class MoveZeroes {
    private MoveZeroes() {}

    public static void moveZeroes(int[] nums) {
        if (nums == null) { return; }

        int replace = 0;
        for (int i = 0; i < nums.length; ++i)
        {
            if (nums[i] != 0)
            {
                nums[replace] = nums[i];
                replace += 1;
            }
        }

        Arrays.fill(nums, replace, nums.length, 0);
    }
}
