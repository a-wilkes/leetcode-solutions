package main.java.leetcode;

// https://leetcode.com/problems/contains-duplicate/

import java.util.Arrays;
import java.util.stream.Collectors;

public class ContainsDuplicate {
    private ContainsDuplicate() {}

    public static boolean containsDuplicate(int[] nums) {
        if (nums == null) { return false; }
        return Arrays.stream(nums).boxed()
                .collect(Collectors.toSet())
                .size() != nums.length;
    }
}
