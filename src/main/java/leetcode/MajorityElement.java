package main.java.leetcode;

import java.util.Arrays;

// https://leetcode.com/problems/majority-element/solution/

public class MajorityElement {
    private MajorityElement() {}

    public static int majorityElement(int[] nums) {
//        Arrays.sort(nums);
//        return nums[nums.length / 2];
        return BoyerMooreVotingAlgorithm(nums);
    }

//    https://en.wikipedia.org/wiki/Boyer%E2%80%93Moore_majority_vote_algorithm
    private static int BoyerMooreVotingAlgorithm(int[] nums) {
        int balance   = 0;
        int selection = nums[0];

        for (final int i : nums) {
            if (balance == 0) { selection = i; }
            balance += (selection == i) ? 1 : -1;
        }

        return selection;
    }
}
