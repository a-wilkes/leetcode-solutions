package main.java.leetcode;

import java.util.Map;

// https://leetcode.com/problems/roman-to-integer/

public class RomanToInteger {
    private RomanToInteger() {}

    public static int romanToInt(String s) {
        final Map<Character, Integer> numerals = Map.of(
                'I',    1, 'V',   5, 'X',  10,
                'L',   50, 'C', 100, 'D', 500,
                'M', 1000
        );

        int res = 0;

        for (int i = s.length() - 2; i >= 0; --i) {
            final int thisValue  = numerals.get(s.charAt(i));
            final int priorValue = numerals.get(s.charAt(i + 1));

            if (thisValue >= priorValue) { res += thisValue; }
            else { res -= thisValue; }
        }

        return res + numerals.get(s.charAt(s.length() - 1));
    }
}
