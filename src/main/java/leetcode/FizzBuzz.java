package main.java.leetcode;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

// https://leetcode.com/problems/fizz-buzz/

public class FizzBuzz {
    private FizzBuzz() {}

    public static List<String> fizzBuzz(int n) {
        return IntStream.range(1, n + 1)
                .mapToObj(i -> {
                    StringBuilder s = new StringBuilder();

                    if (isFizz(i)) { s.append("Fizz"); }
                    if (isBuzz(i)) { s.append("Buzz"); }
                    if (s.length() == 0) { s.append(i); }

                    return s.toString();
                }).collect(Collectors.toList());
    }

    private static boolean isFizz(final int n) {
        return n % 3 == 0;
    }

    private static boolean isBuzz(final int n) {
        return n % 5 == 0;
    }
}
