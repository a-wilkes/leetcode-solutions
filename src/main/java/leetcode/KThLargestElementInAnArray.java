package main.java.leetcode;

import java.util.Arrays;

// https://leetcode.com/problems/kth-largest-element-in-an-array/

public class KThLargestElementInAnArray {
    private KThLargestElementInAnArray() {}

    public static int findKthLargest(int[] nums, int k) {
        Arrays.sort(nums);
        return nums[nums.length - k];
    }
}
